import { Component, OnInit } from '@angular/core';
import { Country } from './country.model';
import { WorldStats } from './world-stats.model';
import { CcovidService } from './ccovid.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  countries = [];
  country: Country;
  worldstats: WorldStats;

  constructor(private covidService: CcovidService) {
   }

  ngOnInit() {
    this.getAllstats();
    // return this.covidService.getCountries('').subscribe(data => this.countries$ = data)

  }

  go = (a) => {
    console.log(a)
  }
  searchcountry = (event: any) => {
    console.log(event)
    this.countries = []
    return this.covidService.getCountries(event).subscribe(data => {
      if (Array.isArray(data)) {
        var j = []
        j.push(data)
        this.countries = j[0]
      } else {
        this.countries.push(data)
      }
    });
  }
  getAllstats = () => {
  return this.covidService.getWorldStats().subscribe(data => {
    if (data) {
      this.worldstats = data;
    }
  });
}
}
