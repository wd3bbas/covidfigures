import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Country } from './country.model';
import { WorldStats } from './world-stats.model';

@Injectable({
  providedIn: 'root'
})
export class CcovidService {
  apiUrl = 'https://disease.sh/v3/covid-19/';
  constructor(private _http: HttpClient) { }
  getCountries(country) {
    return this._http.get<Country>(this.apiUrl + 'countries/' + (country ? country : '?sort=todayCases'));
  }
  getWorldStats() {
    return this._http.get<WorldStats>(this.apiUrl + 'all/');
  }


}
