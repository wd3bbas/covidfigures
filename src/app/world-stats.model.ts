export class WorldStats {
cases: number;
deaths: number;
updated: string;
todayCases: number;
todayDeaths: number;
recovered: number;
}
