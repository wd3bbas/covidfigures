import { TestBed } from '@angular/core/testing';

import { CcovidService } from './ccovid.service';

describe('CcovidService', () => {
  let service: CcovidService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CcovidService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
