export class Country {
    country: String;
    countryInfo: object;
    cases:  String;
    todayCases: String;
    deaths: String;
    todayDeaths: String;
    recovered: String
    active: String;
    critical: String;
    casesPerOneMillion: String;
    deathsPerOneMillion: String;
    updated: String;

}